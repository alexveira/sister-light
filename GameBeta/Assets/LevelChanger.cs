﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour {

	// Use this for initialization

	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "PlayerTrigger") {
			SceneManager.LoadScene ("Fire2");
		}
	}
}
