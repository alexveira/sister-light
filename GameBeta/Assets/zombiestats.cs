﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombiestats : MonoBehaviour {
	public float live;
	public bool iluminated;
	public Color mycolor;
	// Use this for initialization
	void Start () {
		iluminated = false;
		live = 100;
		mycolor = Color.white;
		InvokeRepeating ("NotIluminated", 0.5f, 0.5f);
	}
	void Awake(){
		iluminated = false;
		live = 100;
		mycolor = Color.white;
	}
	void OnEnable(){
		iluminated = false;
		live = 100;
		mycolor = Color.white;

	}
	/*
	void OnLevelWasLoaded(){
		iluminated = false;
		live = 100;
		mycolor = Color.white;
	}
	*/
	// Update is called once per frame
	void Update () {
		Debug.Log(live);
		Debug.Log(live);
		gameObject.GetComponent<SpriteRenderer> ().color = mycolor;
		if (live <= 0) {
			gameObject.SetActive (false);
			Debug.Log(live);
		}
		//iluminated = false;
	}
	void NotIluminated(){
		iluminated = false;
	}
}
