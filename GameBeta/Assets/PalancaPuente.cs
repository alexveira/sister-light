﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalancaPuente : MonoBehaviour {
	public GameObject Puente;
	public bool active;
	float angle;
	public GameObject palancaactivada;
	// Use this for initialization
	void Start () {
		palancaactivada.SetActive(false);
		active = false;
		angle = -45f;
	}

	// Update is called once per frame
	void Update () {
		if(active){
			angle += 25 * Time.deltaTime;
		}
		angle = Mathf.Clamp( angle, -45, 0 );
		Puente.transform.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);

	}
	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "FireFlight") {
			active = true;
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
			palancaactivada.SetActive(true);
		}	
	}
}
