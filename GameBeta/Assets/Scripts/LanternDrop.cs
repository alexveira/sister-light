﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternDrop : MonoBehaviour {
	public GameObject lantern;
	bool spawned;
	float time;
	// Use this for initialization
	void Start () {
		spawned = false;
	}

	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if (gameObject.GetComponent<PlayerMovement> ().haslantern == false && spawned == false) {
			if (gameObject.GetComponent<PlayerMovement> ().ltime > 1f) {
				Spawn ();
				spawned = true;
			}
		}
		if (gameObject.GetComponent<PlayerMovement> ().haslantern == true) {
			spawned = false;
			
		}
	}	
	void Spawn(){
		var spawnedlantern = (GameObject)Instantiate(lantern,gameObject.transform.position,Quaternion.identity);
	}
}
