﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelController : MonoBehaviour {

	public void LoadScene(){
		SceneManager.LoadScene ("Nivel");
	}
	public void LoadTestScene(){
		SceneManager.LoadScene ("TestScene 1");
	}
	public void QuitGame(){
		Application.Quit ();
	}
	public void LoadMenu(){
		SceneManager.LoadScene ("Menu");
	}
	public void LoadLevelsMenu(){
		SceneManager.LoadScene ("MenuNiveles");
	}
	public void LoadLevel3(){
		SceneManager.LoadScene ("Fire1");
	}
	public void LoadLevel4(){
		SceneManager.LoadScene ("Fire2");
	}
}
