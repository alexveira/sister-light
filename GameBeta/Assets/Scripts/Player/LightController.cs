﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {
	public Transform[] TargetRayRight;
	public Transform[] OriginRayRight;
	public GameObject sisterlight;
	Vector2 direction;
	float secodaxis;
	Quaternion t;
	public bool tope;
	void Start () {
		t = sisterlight.transform.rotation * Quaternion.Euler (0, 0, 0);
		tope = true;
	}
	// Update is called once per frame
	void Update () {
		//Debug.Log (GameObject.FindGameObjectWithTag ("Zombie"));
		secodaxis = Input.GetAxis ("Vertical2");
		//Debug.Log (tope);
		if (gameObject.GetComponent<PlayerMovement> ().facingright) {
			tope = false;
			t = sisterlight.transform.rotation * Quaternion.Euler (0, 0, secodaxis * Time.deltaTime * 50f);
			if (t.eulerAngles.z >= 300) {
				tope = true;
				t = Quaternion.Euler (t.eulerAngles.x, t.eulerAngles.y, 300);
			}
			if (t.eulerAngles.z <= 230) {
				tope = true;
				t = Quaternion.Euler (t.eulerAngles.x, t.eulerAngles.y, 230);
			}

		}
			
		if (gameObject.GetComponent<PlayerMovement> ().facingleft) {
			tope = false;
			t = sisterlight.transform.rotation * Quaternion.Euler (0, 0, -secodaxis * Time.deltaTime * 50f);
			if (t.eulerAngles.z > 130) {
				tope = true;
				t = Quaternion.Euler (t.eulerAngles.x, t.eulerAngles.y, 130);
			}
			if (t.eulerAngles.z < 60) {
				tope = true;
				t = Quaternion.Euler (t.eulerAngles.x, t.eulerAngles.y, 60);
			}
		}
		if (!tope) {
			sisterlight.transform.rotation = t;
		}
	}
	void FixedUpdate () {
		for (int i = 0; i < 5; i++) {
			direction = TargetRayRight[i].position - OriginRayRight[i].position;
			if (transform.GetComponent<PlayerMovement> ().sisterLighting) {
				RaycastHit2D hit = Physics2D.Raycast (OriginRayRight[i].position, direction, 5.5f);
				if (hit.collider != null && GameManagerScripts.Instance.Mana > 0f) {
					Debug.DrawRay (OriginRayRight[i].position, direction, Color.yellow);
					//Debug.Log (hit.collider.gameObject.tag);
					if (hit.collider.tag == "Enemy") {
						hit.collider.gameObject.GetComponent<EnemyStats> ().iluminated = true;
					}	
					if (hit.collider.tag == "Zombie") {
						hit.collider.gameObject.GetComponent<zombiestats> ().iluminated = true;
					}
					if (hit.collider.tag == "Romus") {
						hit.collider.gameObject.GetComponent<RomusStats> ().iluminated = true;
					}
				} 
			}
		}
	}
}
