﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

	public GameObject player;
	public GameObject fireflight;     
	private Vector3 offset;         

	// Use this for initialization
	void Start () 
	{
		offset = transform.position - player.transform.position;
	}

	// LateUpdate is called after Update each frame
	void LateUpdate(){
		if (player.GetComponent<PlayerMovement> ().fireflight == false) {
			transform.position = player.transform.position + offset;
		} else {
			transform.position = fireflight.transform.position+ offset;
		}

	}
}