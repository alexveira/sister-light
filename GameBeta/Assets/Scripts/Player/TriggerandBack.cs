﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerandBack : MonoBehaviour {
	public GameObject player;
	public GameObject spriterender;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//StartCoroutine(LateCall());
	}
	IEnumerator LateCall()
	{
		yield return new WaitForSeconds(0.5f);
		spriterender.gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Enemy" || col.gameObject.tag == "Cabesa" || col.gameObject.tag == "Trampa" || col.gameObject.tag == "Zombie") {
			//GameManagerScripts.Instance.IsAlive = false;

			if (player.gameObject.GetComponent<PlayerMovement> ().facingright) {
				player.gameObject.GetComponent<PlayerMovement>().myrb.velocity = new Vector2 (7.4f * 0.5f, 7.4f * 0.5f);
			}
			if (player.gameObject.GetComponent<PlayerMovement> ().facingleft) {
				player.gameObject.GetComponent<PlayerMovement>().myrb.velocity = new Vector2 (-7.4f * 0.5f, 7.4f * 0.5f);
			}
			spriterender.gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
			StartCoroutine(LateCall());
			GameManagerScripts.Instance.Live -= 15f;
		}	
	}

}
