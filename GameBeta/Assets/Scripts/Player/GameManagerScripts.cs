﻿using UnityEngine;
using System.Collections;

public class GameManagerScripts : MonoBehaviour
{
	private static GameManagerScripts _instance;
	public static GameManagerScripts Instance
	{
		get
		{
			if (_instance == null)
			{
				GameObject go = new GameObject("GameManager");
				go.AddComponent<GameManagerScripts>();
			}
			return _instance;
		}
	}
	public bool IsAlive { get; set; }
	public float Live { get; set; }
	public float Mana { get; set; }
	public bool nota1 { get; set; }
	public bool nota2 { get; set; }
	public bool nota3 { get; set; }
	public bool nota4 { get; set; }
	public bool nota5 { get; set; }
	public bool nota6 { get; set; }
	public bool nota7 { get; set; }
	public bool nota8 { get; set; }
	public bool nota9 { get; set; }
	public bool nota10 { get; set; }


	void Awake()
	{
		_instance = this;
		DontDestroyOnLoad (this);
	}
	void Start()
	{
		//Inizializar Variables
		IsAlive = true;
		Live = 100f;
		Mana = 100f;
		/*
		nota1 = false;
		nota2 = false;
		nota3 = false;
		nota4 = false;
		nota5 = false;
		nota6 = false;
		nota7 = false;
		nota8 = false;
		nota9 = false;
		nota10 = false;
		*/
	}
}