﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFlight : MonoBehaviour {
	//public Collider2D FireflightArea;
	public GameObject Player;
	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerExit2D(Collider2D col){
		if (col.gameObject.tag == "FireflightArea") {
			Player.gameObject.GetComponent<PlayerMovement> ().firefligtincollider = false;
			Debug.Log ("Fuera de Rango");
		}
	}
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Enemy" || col.gameObject.tag == "Zombie") {
			Player.gameObject.GetComponent<PlayerMovement> ().firefligtincollider = false;
			Debug.Log ("Tocado Enemigo");
		}
	}
}
