﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRay : MonoBehaviour {
	Transform center;
	public GameObject orbigobject;
	public GameObject player;
	public float radius;
	public Vector3 axis = Vector3.forward;
	public Vector3 desiredPosition;
	public float radiusSpeed = 0.5f;
	float secodaxis;
	// Use this for initialization
	void Start () {
		center = orbigobject.transform;
		transform.position = (transform.position - center.position).normalized * radius + center.position;

	}
	void Update () {
		if (player.GetComponent<LightController> ().tope == false) {
			if (player.GetComponent<PlayerMovement> ().facingright) {
				secodaxis = Input.GetAxis ("Vertical2");
				transform.RotateAround (center.position, axis, secodaxis * Time.deltaTime * 70);
				desiredPosition = (transform.position - center.position).normalized * radius + center.position;
				transform.position = Vector3.MoveTowards (transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
			}
			if (player.GetComponent<PlayerMovement> ().facingleft) {
				secodaxis = Input.GetAxis ("Vertical2");
				transform.RotateAround (center.position, axis, secodaxis * Time.deltaTime * -70);
				desiredPosition = (transform.position - center.position).normalized * radius + center.position;
				transform.position = Vector3.MoveTowards (transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
			}
		}


	

	}
}
