﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombiespawn : MonoBehaviour {
	public GameObject zombie;
	public GameObject Particle;
	public GameObject position;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.tag == "PlayerTrigger") {
			var spawnzombie = (GameObject)Instantiate(zombie,position.transform.position,Quaternion.identity);
			var parti = (GameObject)Instantiate(Particle,position.transform.position,Quaternion.identity);
			//bloque.SetActive(false);
			Destroy (gameObject);
		}
	}

}
