﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlalancaPlayAnim : MonoBehaviour {
	public GameObject palancaactivada;
	public bool active;
	public Animator myAnimator;
	public bool activebyplayer;
	public bool activebyfireflight;
	// Use this for initialization
	void Start () {
		palancaactivada.SetActive(false);
		active = false;
		myAnimator.SetBool ("play", false);
	}

	// Update is called once per frame
	void Update () {


	}
	void OnTriggerEnter2D(Collider2D col){
		if (activebyplayer && !activebyfireflight) {
			if (col.gameObject.tag == "PlayerTrigger") {
				active = true;
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
				palancaactivada.SetActive(true);
				myAnimator.SetBool ("play", true);
			}
		}
		if (activebyfireflight && !activebyplayer) {
			if (col.gameObject.tag == "FireFlightTrigger") {
				active = true;
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
				palancaactivada.SetActive(true);
				myAnimator.SetBool ("play", true);
			}
		}
		if (activebyplayer && activebyfireflight) {
			if (col.gameObject.tag == "PlayerTrigger" || col.gameObject.tag == "FireFlightTrigger") {
				active = true;
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
				palancaactivada.SetActive(true);
				myAnimator.SetBool ("play", true);
			}
		}
	
	}

}
