﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderActivator : MonoBehaviour {
	Transform parent;
	// Use this for initialization
	void Start () {
		//parent = this.gameObject.transform.GetComponentInParent<LightingTemp>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "PlayerTrigger") {
			this.gameObject.transform.GetComponentInParent<LightingTemp>().thunderbool = true;
			//parent.gameObject.GetComponent<LightingTemp> ().thunderbool = true;
			gameObject.GetComponent<BoxCollider2D>().enabled = false;
			Debug.Log ("Thunder");
		}
	}
}
