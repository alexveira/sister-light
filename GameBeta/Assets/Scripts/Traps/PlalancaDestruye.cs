﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlalancaDestruye : MonoBehaviour {
	public GameObject ObjectoDestroy;
	public bool active;
	public GameObject palancaactivada;
	// Use this for initialization
	void Start () {
		palancaactivada.SetActive(false);
		active = false;
	}

	// Update is called once per frame
	void Update () {


	}
	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "FireFlight" || col.gameObject.tag == "PlayerTrigger") {
			active = true;
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
			palancaactivada.SetActive(true);
			Destroy (ObjectoDestroy);
		}	
	}

}
