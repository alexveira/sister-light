﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstaDeadTrigger : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "PlayerTrigger") {
			GameManagerScripts.Instance.IsAlive = false;
		}
	}
}
