﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectileScript : MonoBehaviour {
	Rigidbody2D myrb;
	// Use this for initialization
	void Start () {
		myrb = gameObject.GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {
		myrb.AddForce ((Vector2.left * 50.0f) * Time.deltaTime);
		Destroy (gameObject, 5f);
	}
	void OnCollisionEnter2D (Collision2D col) {
		if (col.gameObject.tag == "Lantern") {
			gameObject.SetActive (false);
		}
	}
}
