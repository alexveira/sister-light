﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTerrimus : MonoBehaviour {
	public GameObject Terrimus;
	bool once;
	// Use this for initializatio

	void Start(){
		once = true;
	}
	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "PlayerTrigger" && once) {
			Terrimus.GetComponent<TerrimusController> ().Inrange = true;
			once = false;
		}
	}
}
