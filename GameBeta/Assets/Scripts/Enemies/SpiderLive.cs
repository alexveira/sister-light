﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderLive : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (gameObject.GetComponent<EnemyStats> ().iluminated == true && gameObject.GetComponent<EnemyStats> ().live > 0) {
			gameObject.GetComponent<EnemyStats> ().live -= 45f * Time.deltaTime;
		}
	}
}
