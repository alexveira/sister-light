﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrimusController : MonoBehaviour {
	public Animator myAnimator;
	public GameObject MyCollider;
	public bool Inrange;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<BoxCollider2D>().enabled = false;
		MyCollider.GetComponent<CircleCollider2D>().enabled = false;
		myAnimator = gameObject.GetComponent<Animator> ();
		Inrange = false;
		myAnimator.SetBool ("Spot", false);
		myAnimator.SetBool ("Free", false);
		myAnimator.SetBool ("Attack", false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Inrange) {
			Invoke ("Out", 1f);
			myAnimator.SetBool ("Spot", true);
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
			Inrange = false;
		}
	}
	void Out(){
		MyCollider.GetComponent<CircleCollider2D>().enabled = true;
		Invoke ("Free", 2f);
	}
	void Free(){
		myAnimator.SetBool ("Free", true);
		MyCollider.GetComponent<CircleCollider2D>().enabled = false;
	}
}
