﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spectrant : MonoBehaviour {
	public float pasos;
	public bool cambio;
	public bool derecha = true;
	// Use this for initialization
	void Start () {
		cambio = false;
	}
	// Update is called once per frame
	void Update () {
		if(gameObject.GetComponent<EnemyStats> ().iluminated == true && gameObject.GetComponent<EnemyStats> ().live > 0){
			gameObject.GetComponent<EnemyStats> ().live -= 45f * Time.deltaTime;
			gameObject.GetComponent<EnemyStats> ().mycolor.g -= 0.2f * Time.deltaTime;
			gameObject.GetComponent<EnemyStats> ().mycolor.b -= 0.2f * Time.deltaTime;
		}
		if (cambio) {
			transform.position = transform.position + new Vector3 (-pasos, 0, 0) * Time.deltaTime;
			if (derecha == true) {
				Giro ();
			}
			derecha = false;
		} else {
			transform.position = transform.position + new Vector3 (pasos, 0, 0) * Time.deltaTime;
			if (derecha == false) {
				Giro ();
			}
			derecha = true;
		}
	}
	void OnCollisionStay2D (Collision2D col) {

		if (col.gameObject.tag != "Floor") {
			cambio = !cambio;
			//Debug.Log ("cambio");
		}
	}
	void Giro(){
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
}
