﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectrantTrigger : MonoBehaviour {
	public GameObject parent;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
	void OnTriggerEnter2D (Collider2D col) {

		if (col.gameObject.tag != "Floor") {
			parent.GetComponent<Spectrant>().cambio = !parent.GetComponent<Spectrant>().cambio;
		}
	}
}