﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arsarbone : MonoBehaviour {
	public GameObject cabesa;
	float time;
	public float limit;
	public GameObject miexplosion;
	public bool Inrange;
	public Animator myAnimator;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Inrange) {
			myAnimator.SetBool ("Attack", true);
			time += Time.deltaTime;
			if(time >= limit){
				var spawnedlantern = (GameObject)Instantiate(cabesa,gameObject.transform.position + new Vector3(-1.5f,0.7f,0.0f),Quaternion.identity);
				time = 0;
			}
		}
	}
	void OnCollisionEnter2D (Collision2D col) {
		if (col.gameObject.tag == "Lantern") {
			var Explosion = (GameObject)Instantiate(miexplosion,gameObject.transform.position,Quaternion.identity);
			gameObject.SetActive (false);
		}
	}

}
