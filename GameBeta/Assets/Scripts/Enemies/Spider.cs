﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MonoBehaviour {
	public GameObject Player;
	public GameObject Spiderlights;
	public bool traped;
	private Rigidbody2D myrb;
	private BoxCollider2D mybc;
	public Animator myAnimator;
	// Use this for initialization
	void Start () {
		traped = false;
		myrb = gameObject.GetComponent<Rigidbody2D> ();
		mybc = gameObject.GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (traped) {
			Spiderlights.SetActive (true);
			myAnimator.SetBool ("Attack", true);
			Player.GetComponent<PlayerMovement> ().lockmovement = true;
			Player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 1);
			myrb.velocity = new Vector2 (0, 0.5f);
		} else {
			myAnimator.SetBool ("Attack" ,false);
			Spiderlights.SetActive (false);
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "PlayerTrigger") {
			Player.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0f, 0f, 0f);
			traped = true;
		}
		if (col.gameObject.tag == "FireFlightTrigger" && traped) {
			traped = false;
			Player.GetComponent<PlayerMovement> ().lockmovement = false;
			mybc.enabled = !mybc.enabled;
		}
	}
}
