﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostTrigger : MonoBehaviour {
	public GameObject parent;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
	void OnTriggerEnter2D (Collider2D col) {

		if (col.gameObject.tag != "Floor") {
			parent.GetComponent<GhostMovement>().cambio = !parent.GetComponent<GhostMovement>().cambio;
		}
	}
}
