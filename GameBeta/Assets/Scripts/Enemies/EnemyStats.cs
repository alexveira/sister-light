﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {
	public float live;
	public bool iluminated;
	public Color mycolor;
	public bool change;
	public GameObject miexplosion;
	// Use this for initialization
	void Start () {
		iluminated = false;
		live = 100;
		mycolor = Color.white;
		InvokeRepeating ("NotIluminated", 0.5f, 0.5f);
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log(live);
		gameObject.GetComponent<SpriteRenderer> ().color = mycolor;
		if (live <= 0) {
			if (change) {
				var Explosion2 = (GameObject)Instantiate (miexplosion, gameObject.transform.position + new Vector3 (1.5f, -0.5f, 0.0f), Quaternion.identity);
			} else {
				var Explosion = (GameObject)Instantiate(miexplosion,gameObject.transform.position,Quaternion.identity);
			}
			gameObject.SetActive (false);
			Debug.Log("Un Muerto");
		}
		//iluminated = false;
	}
	void FixedUpdate(){
		//iluminated = false;
	}

	void NotIluminated(){
		iluminated = false;
	}
}
