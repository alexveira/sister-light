﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RomusMovement1 : MonoBehaviour {
	float live;
	public GameObject romusbola;
	public GameObject colliderobjeto;
	public BoxCollider2D collider;
	public GameObject colliderdaño;
	public Animator myRomusAnimator;
	public bool iluminated;
	public float velocidad;
	public bool cambio;
	public bool derecha = true;
	// Use this for initialization
	void Start () {
		cambio = false;
		live = 100;
		myRomusAnimator.SetBool ("ball", false);
		velocidad = 1.2f;
	}
	// Update is called once per frame
	void Update () {
		if (colliderobjeto.GetComponent<RomusStats> ().iluminated == true) {
			myRomusAnimator.SetBool ("ball", true);
			velocidad = 0f;
			colliderdaño.SetActive(false);
			collider.enabled = true;
		} else {
			myRomusAnimator.SetBool ("ball", false);
			velocidad = 1.2f;
			colliderdaño.SetActive(true);
			collider.enabled = false;
		}
		//Debug.Log (gameObject.GetComponent<EnemyStats> ().live);
		if (cambio) {
			transform.position = transform.position + new Vector3 (-velocidad, 0, 0) * Time.deltaTime;
			if (derecha == true) {
				Giro ();
			}
			derecha = false;
		} else {
			transform.position = transform.position + new Vector3 (velocidad, 0, 0) * Time.deltaTime;
			if (derecha == false) {
				Giro ();
			}
			derecha = true;
		}
	}
	void OnCollisionStay2D (Collision2D col) {

		if (col.gameObject.tag != "Floor" && col.gameObject.tag != "Player") {
			cambio = !cambio;
			Debug.Log ("cambio");
		}
	}
	void Giro(){
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
}