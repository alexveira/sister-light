﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieMovement : MonoBehaviour {

	public float pasos;
	public bool cambio;
	public bool derecha = true;
	// Use this for initialization
	void Start () {
		cambio = false;
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log (gameObject.GetComponent<EnemyStats> ().live);
		//Debug.Log (gameObject.GetComponent<EnemyStats> ().iluminated);
		if(gameObject.GetComponent<zombiestats> ().iluminated == true/* && gameObject.GetComponent<zombiestats> ().live > 0*/){
			gameObject.GetComponent<zombiestats> ().live -= 25f * Time.deltaTime;
			gameObject.GetComponent<zombiestats> ().mycolor.g -= 0.2f * Time.deltaTime;
			gameObject.GetComponent<zombiestats> ().mycolor.b -= 0.2f * Time.deltaTime;
			Debug.Log ("Me estan matando");

		}
		if (cambio) {
			transform.position = transform.position + new Vector3 (-pasos, 0, 0) * Time.deltaTime;
			if (derecha == true) {
				Giro ();
			}
			derecha = false;
		} else {
			transform.position = transform.position + new Vector3 (pasos, 0, 0) * Time.deltaTime;
			if (derecha == false) {
				Giro ();
			}
			derecha = true;
		}
	}
	void OnCollisionStay2D (Collision2D col) {

		if (col.gameObject.tag != "Floor") {
			cambio = !cambio;
			//Debug.Log ("cambio");
		}
	}
	void Giro(){
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
	void FixedUpdate(){
		//gameObject.GetComponent<zombiestats> ().iluminated = false;
	}
}