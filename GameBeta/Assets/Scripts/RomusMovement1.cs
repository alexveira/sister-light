﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RomusMovement1 : MonoBehaviour {
	float live;
	public bool iluminated;
	public GameObject romusbola;
	public GameObject andando;
	public float velocidad;
	public bool cambio;
	public bool derecha = true;
	// Use this for initialization
	void Start () {
		cambio = false;
		live = 100;
		romusbola.SetActive (false);
		andando.SetActive (false);
	}
	// Update is called once per frame
	void Update () {
		if (gameObject.GetComponent<EnemyStats> ().iluminated == true && gameObject.GetComponent<EnemyStats> ().live > 0) {
			//gameObject.GetComponent<EnemyStats> ().live--;
			velocidad = 0;
			romusbola.SetActive (false);
			andando.SetActive (true);
		} else {
			andando.SetActive (false);
			romusbola.SetActive (true);
			velocidad = 1.2f;
		}
		//Debug.Log (gameObject.GetComponent<EnemyStats> ().live);
		if (cambio) {
			transform.position = transform.position + new Vector3 (-velocidad, 0, 0) * Time.deltaTime;
			if (derecha == true) {
				Giro ();
			}
			derecha = false;
		} else {
			transform.position = transform.position + new Vector3 (velocidad, 0, 0) * Time.deltaTime;
			if (derecha == false) {
				Giro ();
			}
			derecha = true;
		}
	}
	void OnCollisionStay2D (Collision2D col) {

		if (col.gameObject.tag != "Floor") {
			cambio = !cambio;
			Debug.Log ("cambio");
		}
	}
	void Giro(){
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
}