﻿﻿using UnityEngine;
using System.Collections;

public class GameManagerScripts : MonoBehaviour
{
	private static GameManagerScripts _instance;
	public static GameManagerScripts Instance
	{
		get
		{
			if (_instance == null)
			{
				GameObject go = new GameObject("GameManager");
				go.AddComponent<GameManagerScripts>();
			}
			return _instance;
		}
	}
	public bool IsAlive { get; set; }
	public int Live { get; set; }


	void Awake()
	{
		_instance = this;
	}
	void Start()
	{
		//Inizializar Variables
		IsAlive = true;
		Live = 100;
	}
}