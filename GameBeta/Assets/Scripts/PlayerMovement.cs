﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {
	public Rigidbody2D myrb;
	public GameObject AnimatorObject;
	public bool fireflight;
	Animator myAnimator;
	public GameObject fireFlightIdle;
	public GameObject fireFlight;
	public GameObject sisterLight;
	public GameObject spriterender;
	public LayerMask groundLayer;
	public GameObject fireflightidle;
	public float jumpimpulse;
	public float speed;
	public float lanternSpeed;
	public float lanternMaxSpeed;
	public float maxspeed;
	float initialMaxSpeed;
	float initialspeed;
	private float vInput, hInput, rTrigger;
	private Vector2 movement;
	public bool grounded;
	public bool facingright;
	public bool facingleft;
	public bool sisterLighting;
	public bool haslantern;
	private Vector2 fireflihtmovement;
	public GameObject defaultligth;
	public float ltime = 0f;
	public GameObject GameOverText;
	public bool firefligtincollider;
	// Use this for initialization
	void Start () {
		firefligtincollider = true;
		Time.timeScale = 1;
		GameOverText.SetActive(false);
		fireflight = false;
		myrb = gameObject.GetComponent<Rigidbody2D> ();
		initialspeed = speed;
		initialMaxSpeed = maxspeed;
		fireFlight.SetActive (false);
		myAnimator = AnimatorObject.GetComponent<Animator> ();
		GameManagerScripts.Instance.IsAlive = true;
	}
	// Update is called once per frame
	void Update () {
		Debug.Log (GameManagerScripts.Instance.Live);
		if (GameManagerScripts.Instance.IsAlive == false) {
			Time.timeScale = 0;
			GameOverText.SetActive (true);
		}
		if (myrb.velocity == Vector2.zero) {
			myAnimator.SetBool ("Runing", false);
		} else {
				myAnimator.SetBool ("Runing", true);
		}
		//Fireflight

		if (Input.GetButton ("XboxY") && haslantern && firefligtincollider && grounded) {
			fireflightidle.SetActive (false);
			defaultligth.SetActive (false);
			firefligtincollider = true;
			fireflight = true;
			if (!firefligtincollider) {
				Debug.Log ("Vuelve");
				fireflight = false;
				fireFlight.transform.localPosition = new Vector3 (0, 0, 0);
			}
		} else {
			fireflightidle.SetActive (true);
			defaultligth.SetActive (true);
			fireflight = false;
			fireFlight.transform.localPosition = new Vector3 (0, 0, 0);
			firefligtincollider = true;
		}
		if (!fireflight) {
			fireFlight.SetActive (false);
			if (haslantern) {
				
				fireFlightIdle.SetActive (true);
				ltime += Time.deltaTime;
			} else {
				fireFlightIdle.SetActive (false);
				ltime = 0f;
			}
			if (Input.GetButtonUp ("XboxB") && haslantern) {
				if (ltime > 1f) {
					//Debug.Log ("soltado");
					haslantern = false;
				}
			}
			if (hInput > 0.15f) {
				facingright = true;
				facingleft = false;
				transform.localScale = new Vector3 (3f, 5, 0);
			} else if (hInput < -0.15f) {
				facingright = false;
				facingleft = true;
				transform.localScale = new Vector3 (-3f, 5, 0);
			}
			if (haslantern) {
				myAnimator.SetBool ("HasLantern", true);
				sisterLight.SetActive (false);
				//SisterLight
				if (rTrigger > 0f) {
					if (grounded) {
						sisterLight.SetActive (true);
						sisterLighting = true;
						speed = lanternSpeed;
						maxspeed = lanternMaxSpeed;
					}
				} else {
					sisterLighting = false;
					speed = initialspeed;
					maxspeed = initialMaxSpeed;
				}
			} else {
				myAnimator.SetBool ("HasLantern", false);
			}
		} else {
			fireflihtmovement = (transform.up * vInput) + (transform.right * hInput);
			fireFlight.SetActive (true);
		}
	}
	void FixedUpdate () {
		//Axis
		rTrigger = Input.GetAxisRaw ("RightTrigger"); 
		hInput = Input.GetAxis ("Horizontal");
		vInput = Input.GetAxis ("Vertical");
		if (!fireflight) {
			//Jump
			if (Input.GetButton ("Jump") && grounded && !sisterLighting) {
				//Debug.Log ("Salto");
				//myrb.AddForce (new Vector2(0,jumpimpulse), ForceMode2D.Impulse);
				myrb.velocity = new Vector2 (0, jumpimpulse);
			}
			//Movement
			myrb.AddForce ((Vector2.right * speed) * hInput * Time.deltaTime);
			if (myrb.velocity.x > maxspeed) {
				myrb.velocity = new Vector2 (maxspeed, myrb.velocity.y);
			}
			if (myrb.velocity.x < -maxspeed) {
				myrb.velocity = new Vector2 (-maxspeed, myrb.velocity.y);
			}
			//GroundDetection
			if (facingright) {
				Debug.DrawRay (transform.position + new Vector3 (0.17f, 0.0f, 0.0f), Vector2.down * 0.7f, Color.red);
				Debug.DrawRay (transform.position + new Vector3 (-0.33f, 0.0f, 0.0f), Vector2.down * 0.7f, Color.white);
				if (Physics2D.Raycast (transform.position + new Vector3 (0.17f, 0.0f, 0.0f), Vector2.down, 0.7f, groundLayer) || 
				Physics2D.Raycast (transform.position + new Vector3(-0.33f, 0.0f, 0.0f), Vector2.down, 0.7f, groundLayer)) {
					grounded = true;
					myAnimator.SetBool ("Jumping", false);
				} else {
					grounded = false;
					myAnimator.SetBool ("Jumping", true);
				}
			}
			if (facingleft) {
				Debug.DrawRay (transform.position + new Vector3 (0.32f, 0.0f, 0.0f), Vector2.down * 0.7f, Color.red);
				Debug.DrawRay (transform.position + new Vector3 (-0.17f, 0.0f, 0.0f), Vector2.down * 0.7f, Color.white);
				if (Physics2D.Raycast (transform.position + new Vector3 (0.32f, 0.0f, 0.0f), Vector2.down, 0.7f, groundLayer) || 
				Physics2D.Raycast (transform.position + new Vector3(-0.17f, 0.0f, 0.0f), Vector2.down, 0.7f, groundLayer)) {
					grounded = true;
					myAnimator.SetBool ("Jumping", false);
				} else {
					grounded = false;
					myAnimator.SetBool ("Jumping", true);
				}
			}
		} else {
			fireFlight.GetComponent<Rigidbody2D>().AddForce ((fireflihtmovement * speed/2) * Time.deltaTime);

			if (fireFlight.GetComponent<Rigidbody2D>().velocity.x > maxspeed/2) {
				fireFlight.GetComponent<Rigidbody2D>().velocity = new Vector2 (maxspeed/2, fireFlight.GetComponent<Rigidbody2D>().velocity.y);
			}
			if (fireFlight.GetComponent<Rigidbody2D>().velocity.x < -maxspeed/2) {
				fireFlight.GetComponent<Rigidbody2D>().velocity = new Vector2 (-maxspeed/2, fireFlight.GetComponent<Rigidbody2D>().velocity.y);
			}

			if (fireFlight.GetComponent<Rigidbody2D>().velocity.y > maxspeed/2) {
				fireFlight.GetComponent<Rigidbody2D>().velocity = new Vector2 (fireFlight.GetComponent<Rigidbody2D>().velocity.x, maxspeed/2);
			}
			if (fireFlight.GetComponent<Rigidbody2D>().velocity.y < -maxspeed) {
				fireFlight.GetComponent<Rigidbody2D>().velocity = new Vector2 (fireFlight.GetComponent<Rigidbody2D>().velocity.x, maxspeed/2);
			}

		}
	}
	IEnumerator LateCall()
	{
		yield return new WaitForSeconds(0.5f);
		spriterender.gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
	}
	void OnCollisionStay2D(Collision2D col){
		if (col.gameObject.tag == "Lantern" && Input.GetButtonDown ("XboxB") && !haslantern) {
				haslantern = true;
				ltime = 0f;
				//Debug.Log ("Recogido");
				col.gameObject.SetActive (false);
		}
	}
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Enemy" || col.gameObject.tag == "Cabesa" || col.gameObject.tag == "Trampa" || col.gameObject.tag == "Zombie") {
			//GameManagerScripts.Instance.IsAlive = false;
			//GameManagerScripts.Instance.Live -= 30;
			if(facingright){
				myrb.velocity = new Vector2 (-jumpimpulse * 0.5f, jumpimpulse * 0.5f);
			}
			if(facingleft){
				myrb.velocity = new Vector2 (jumpimpulse * 0.5f, jumpimpulse * 0.5f);
			}
			GameManagerScripts.Instance.Live -= 30;
			spriterender.gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
			StartCoroutine(LateCall());
		}	
	}
}
