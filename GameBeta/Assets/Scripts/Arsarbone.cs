﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arsarbone : MonoBehaviour {
	public GameObject cabesa;
	float time;
	public float limit;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if(time >= limit){
			var spawnedlantern = (GameObject)Instantiate(cabesa,gameObject.transform.position + new Vector3(-0.5f, 0.0f,0.0f),Quaternion.identity);
			time = 0;
		}
	}
	void OnCollisionEnter2D (Collision2D col) {
		if (col.gameObject.tag == "Lantern") {
			gameObject.SetActive (false);
		}
	}
}
