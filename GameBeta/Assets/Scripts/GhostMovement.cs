﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMovement: MonoBehaviour {

	public float pasos;
	public bool cambio;
	public bool derecha = true;
	// Use this for initialization
	void Start () {
		cambio = false;
	}
	// Update is called once per frame
	void Update () {
		if(gameObject.GetComponent<EnemyStats> ().iluminated == true && gameObject.GetComponent<EnemyStats> ().live > 0){
			gameObject.GetComponent<EnemyStats> ().live -= 45f * Time.deltaTime;
			if (!cambio) {
				transform.localScale += new Vector3 (-0.55f, -0.55f, -0.55f) * Time.deltaTime;
			} else {
				transform.localScale += new Vector3 (0.55f, -0.55f, -0.55f) * Time.deltaTime;
			}
			//Debug.Log (gameObject.GetComponent<EnemyStats> ().live);

		}
		if (cambio) {
			transform.position = transform.position + new Vector3 (-pasos, 0, 0) * Time.deltaTime;
			if (derecha == true) {
				Giro ();
			}
			derecha = false;
		} else {
			transform.position = transform.position + new Vector3 (pasos, 0, 0)* Time.deltaTime;
			if (derecha == false) {
				Giro ();
			}
			derecha = true;
		}
	}
	void OnCollisionStay2D (Collision2D col) {

		if (col.gameObject.tag != "Floor") {
			cambio = !cambio;
			//Debug.Log ("cambio");
		}
	}
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Player") {
			GameManagerScripts.Instance.Live -= 30;
		}
	}
	void Giro(){
		transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
}