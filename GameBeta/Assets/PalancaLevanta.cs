﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalancaLevanta : MonoBehaviour {
	public GameObject Bloque;
	public bool active;
	public GameObject palancaactivada;
	// Use this for initialization
	void Start () {
		palancaactivada.SetActive(false);
		active = false;
	}

	// Update is called once per frame
	void Update () {
		if(active){
			if (Bloque.transform.position.y < -20.5f) {
				Bloque.transform.position += new Vector3 (0.0f, 0.5f, 0.0f) * Time.deltaTime;
			}
		}
	}
	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "FireFlight" || col.gameObject.tag == "PlayerTrigger") {
			active = true;
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
			palancaactivada.SetActive(true);
		}	
	}
}