﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingTemp : MonoBehaviour {
	public float sec = 0.1f;
	Transform light;
	public AudioClip thunder;
	AudioSource myaudioSource;
	Transform lightingColliderTransform;
	Collider2D lightingcollider;
	public bool thunderbool;
	// Use this for initialization
	void Start () {
		thunderbool = false;
		gameObject.GetComponent<Renderer>().enabled = false;
		light = this.gameObject.transform.GetChild(0);
		lightingColliderTransform = this.gameObject.transform.GetChild(1);
		lightingcollider = lightingColliderTransform.gameObject.GetComponent<Collider2D>();
		light.GetComponent<MeshRenderer> ().enabled = false;
		myaudioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (thunderbool == true){
			StartCoroutine(LateCall());
			Debug.Log ("Thunder");
			thunderbool = false;
		}
	}
	IEnumerator LateCall()
	{
		Debug.Log ("Latecal");
		yield return new WaitForSeconds(sec);
		myaudioSource.PlayOneShot (thunder, 1.0f);
		gameObject.GetComponent<Renderer>().enabled = true;
		light.gameObject.GetComponent<MeshRenderer> ().enabled = true;
		StartCoroutine(Disable());
	}
	IEnumerator Disable(){
		Debug.Log ("DesableRenderer");
		yield return new WaitForSeconds (0.5f);
		gameObject.GetComponent<Renderer>().enabled = false;
		light.gameObject.GetComponent<MeshRenderer> ().enabled = false;
	}
}
